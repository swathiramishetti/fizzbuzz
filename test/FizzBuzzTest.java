import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class FizzBuzzTest {

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	public void testDivisibleBy3() {
		FizzBuzz b = new FizzBuzz();
		String result = b.buzzzzzz(3);
		assertEquals("fizz", result);
	}
	@Test
	public void testDivisibleBy5() {
		FizzBuzz b = new FizzBuzz();
		String result = b.buzzzzzz(5);
		assertEquals("buzz", result);
	}
	@Test
	public void testDivisibleBy3and5() {
		FizzBuzz b = new FizzBuzz();
		String result = b.buzzzzzz(15);
		assertEquals("fizzbuzz", result);
	}

	@Test
	public void testOtherNumber() {
		FizzBuzz b = new FizzBuzz();
		String result = b.buzzzzzz(4);
		assertEquals("4", result);
	}
}
